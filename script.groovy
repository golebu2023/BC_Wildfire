#!/usr/bin/env

def increment(){
    echo "increment..."
    env.WORKSPACE = pwd()
    matcher = readFile("${env.WORKSPACE}/version.xml")
    def list = matcher.split(",")

    major = list[0]
    minor = list[1]
    patch = list[2]
    patch = patch as Integer
    patch = patch + 1
}

def buildImage(){
    echo "Building Image....d."
    sh "docker-compose build"
}

def dockerhubLogin(){
    echo "Login to dockerhub...."
    withCredentials([usernamePassword(credentialsId:"dockerhub-credentials", usernameVariable: "USER", passwordVariable: "PASS")]){
        sh """
            echo ${PASS} | docker login --username ${USER} --password-stdin && \
            docker tag web:1.0.0 golebu2023/image-registry:web-${major}.${minor}.${patch} && \
            docker tag ui:1.0.0 golebu2023/image-registry:ui-${major}.${minor}.${patch} && \
            docker push golebu2023/image-registry:web-${major}.${minor}.${patch} && \
            docker push golebu2023/image-registry:ui-${major}.${minor}.${patch}
        """

        //Update version.xml
        writeFile(file: "${env.WORKSPACE}/version.xml", text: "${major},${minor},${patch}", encoding: "UTF-8")

    }
}

def deploy(){
    echo "Deploying to live server..."
}

def updateCommit(){
    /********************Update Commit on Repo************************/
    patch = patch as Integer


    if (patch == 1) {
        echo "Called......"
        sh "git config --global user.email 'bcwildfire-project@gmail.com'"
        sh 'git config --global user.name "bcwildfire-project"'
        sh "git config --list"
    }

    withCredentials([usernamePassword(credentialsId: "gitlab-credentials", usernameVariable: "USER", passwordVariable: "PASS")]){
        sh """
            git add . && \
            git commit -m 'ci: jenkins version modified' && \
            git remote set-url origin https://${USER}:${PASS}@gitlab.com/golebu2023/BC_Wildfire.git
            git push origin HEAD:main
        """
    }
}

return this
